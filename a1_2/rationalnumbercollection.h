#include "rationalnumber.h"

typedef struct{
 RationalNumber number;
 int count;
} Element;

typedef struct {
  Element arr[1000];
  int index;
  int total;
  RationalNumber sum;
} RationalNumberCollection;

int rncCount(RationalNumberCollection* rnc, RationalNumber n);

int rncTotalCount(RationalNumberCollection* rnc);

int rncTotalUniqueCount(RationalNumberCollection* rnc);

RationalNumber rncSum(RationalNumberCollection* rnc);

void rncRemove(RationalNumberCollection* rnc, RationalNumber n);

RationalNumber rncAverage(RationalNumberCollection* rnc);

void rncInit(RationalNumberCollection* rnc);

void rncAdd(RationalNumberCollection* rnc, RationalNumber n);

void rncPrint(RationalNumberCollection* rnc);
