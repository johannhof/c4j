/*
  Simple Unit Test for type RationalNumberCollection
*/

#include <stdio.h>
#include <assert.h>

#include "rationalnumbercollection.h"

void testRncInit(){
  printf("\tPerforming unit tests for rncInit()...\n");
  RationalNumberCollection rnc;
  rncInit(&rnc);

  assert(rnc.index == 0);
  printf("\tsuccessful!\n");
}

void testRncAdd(){
  printf("\tPerforming unit tests for rncAdd()...\n");
  RationalNumberCollection rnc;
  rncInit(&rnc);
  RationalNumber n1={1,2};
  rncAdd(&rnc, n1);

  assert(rnc.index == 1);

  RationalNumber n2={2,3};
  rncAdd(&rnc, n2);
  assert(rnc.index == 2);

  RationalNumber n3={1,3};
  rncAdd(&rnc, n3);

  assert(rnc.index == 3);
  assert(rnEqual(rnc.arr[0].number, n3));
  assert(rnEqual(rnc.arr[1].number, n1));
  assert(rnEqual(rnc.arr[2].number, n2));

  printf("\tsuccessful!\n");
}

void testRncCount(){
  printf("\tPerforming unit tests for rncCount()...\n");
  RationalNumberCollection rnc;
  rncInit(&rnc);
  RationalNumber n1={1,2};
  rncAdd(&rnc, n1);

  RationalNumber n2={2,3};
  rncAdd(&rnc, n2);

  RationalNumber n3={1,2};
  rncAdd(&rnc, n3);

  RationalNumber n4={2,4};
  rncAdd(&rnc, n4);

  RationalNumber n5={1,3};

  assert(rncCount(&rnc,n1) == 3);
  assert(rncCount(&rnc,n5) == 0);

  printf("\tsuccessful!\n");
}

void testRncTotalCount(){
  printf("\tPerforming unit tests for rncTotalCount()...\n");
  RationalNumberCollection rnc;
  rncInit(&rnc);

  RationalNumber n1={1,2};
  assert(rncTotalCount(&rnc) == 0);
  rncAdd(&rnc, n1);
  assert(rncTotalCount(&rnc) == 1);

  RationalNumber n2={2,3};
  rncAdd(&rnc, n2);

  RationalNumber n3={1,2};
  rncAdd(&rnc, n3);

  RationalNumber n4={2,4};
  rncAdd(&rnc, n4);

  RationalNumber n5={1,3};
  rncAdd(&rnc, n5);

  assert(rncTotalCount(&rnc) == 5);

  printf("\tsuccessful!\n");
}

void testRncTotalUniqueCount(){
  printf("\tPerforming unit tests for rncTotalUniqueCount()...\n");
  RationalNumberCollection rnc;
  rncInit(&rnc);

  RationalNumber n1={1,2};
  assert(rncTotalUniqueCount(&rnc) == 0);
  rncAdd(&rnc, n1);
  assert(rncTotalUniqueCount(&rnc) == 1);

  RationalNumber n2={2,3};
  rncAdd(&rnc, n2);

  RationalNumber n3={1,2};
  rncAdd(&rnc, n3);

  RationalNumber n4={2,4};
  rncAdd(&rnc, n4);

  RationalNumber n5={1,3};
  rncAdd(&rnc, n5);

  assert(rncTotalUniqueCount(&rnc) == 3);

  printf("\tsuccessful!\n");
}

void testRncSum(){
  printf("\tPerforming unit tests for rncSum()...\n");
  RationalNumberCollection rnc;
  rncInit(&rnc);

  RationalNumber n1={1,2};
  rncAdd(&rnc, n1);

  RationalNumber n3={1,2};
  rncAdd(&rnc, n3);

  RationalNumber n4={2,4};
  rncAdd(&rnc, n4);

  RationalNumber n5={3,2};

  RationalNumber sum = rncSum(&rnc);
  assert(rnEqual(sum, n5));

  printf("\tsuccessful!\n");
}

void testRncAverage(){
  printf("\tPerforming unit tests for rncAverage()...\n");
  RationalNumberCollection rnc;
  rncInit(&rnc);

  RationalNumber n1={1,1};
  rncAdd(&rnc, n1);

  RationalNumber n3={2,1};
  rncAdd(&rnc, n3);

  RationalNumber n4={3,1};
  rncAdd(&rnc, n4);

  RationalNumber n5={2,1};

  RationalNumber sum = rncAverage(&rnc);
  assert(rnEqual(sum, n5));

  printf("\tsuccessful!\n");
}

void testRncRemove(){
  printf("\tPerforming unit tests for rncRemove()...\n");
  RationalNumberCollection rnc;
  rncInit(&rnc);

  RationalNumber n0={1,1};
  rncAdd(&rnc, n0);

  RationalNumber n1={1,1};
  rncAdd(&rnc, n1);

  assert(rnc.arr[0].count == 2);

  RationalNumber n3={2,1};
  rncAdd(&rnc, n3);

  RationalNumber n4={3,1};
  rncAdd(&rnc, n4);

  rncRemove(&rnc, n3);
  assert(rnEqual(rnc.arr[1].number, n4));

  rncRemove(&rnc, n1);
  assert(rnc.arr[0].count == 1);

  printf("\tsuccessful!\n");
}


int main(){
  printf("Performing unit tests for RationalNumberCollection...\n");

  testRncInit();
  testRncAdd();
  testRncCount();
  testRncTotalCount();
  testRncTotalUniqueCount();
  testRncSum();
  testRncAverage();
  testRncRemove();

  printf(" successful!\n");

  return 0;
}

