#include "rationalnumber.h"
#include <stdio.h>

// utility functions
int euclid(int a, int b);
RationalNumber* normalize(RationalNumber* n);
void sameDenominator(RationalNumber* a, RationalNumber* b);

/**
 * "normalizes" the given rationalnumber, finding the smallest positive denominator
 */
RationalNumber* normalize(RationalNumber* n){
  int gcd = euclid(n->numerator, n->denominator);
  if(gcd != 0){
    n->numerator /= gcd;
    n->denominator /= gcd;
  }
  if(n->denominator < 0){
    n->numerator *= -1;
    n->denominator *= -1;
  }
  return n;
}

/**
 * recursive helper function to find the smallest denominator
 */
int euclid(int a, int b){
  if(b == 0){
    return a;
  }else{
    return euclid(b, a % b);
  }
}

/**
 * transform two rationalnumbers to have the same denominator
 */
void sameDenominator(RationalNumber* a, RationalNumber* b){
  int ad = a->denominator;

  a->numerator *= b->denominator;
  a->denominator *= b->denominator;

  b->numerator *= ad;
  b->denominator *= ad;
}

/**
 * Checks if the denominator of a rationalnumber is not 0
 */
char rnIsValid(RationalNumber n){
  return n.denominator != 0;
}

/**
 * checks two rationalnumbers for equality
 * normalizes the numbers, so that e.g. 6/8 == 3/4
 */
char rnEqual(RationalNumber a, RationalNumber b){
  normalize(&a);
  normalize(&b);
  return a.denominator == b.denominator && a.numerator == b.numerator;
}

/**
 * compares two rationalnumbers
 * normalizes the numbers, so that e.g. 5/8 < 3/4
 */
char rnLessThan(RationalNumber a, RationalNumber b){
  sameDenominator(normalize(&a),normalize(&b));
  return a.numerator < b.numerator;
}

/**
 * Adds one rationalnumber to another
 */
RationalNumber rnAdd(RationalNumber a, RationalNumber b){
  sameDenominator(&a,&b);
  RationalNumber r = {a.numerator + b.numerator, a.denominator};
  return *normalize(&r);
}

/**
 * Subtracts one rationalnumber from another
 */
RationalNumber rnSubtract(RationalNumber a, RationalNumber b){
  sameDenominator(&a,&b);
  RationalNumber r = {a.numerator - b.numerator, a.denominator};
  return *normalize(&r);
}

/**
 * Multiplies one rationalnumber with another
 */
RationalNumber rnMultiply(RationalNumber a, RationalNumber b){
  RationalNumber r = {a.numerator * b.numerator, a.denominator * b.denominator};
  return *normalize(&r);
}

/**
 * Divides one rationalnumber with another
 */
RationalNumber rnDivide(RationalNumber a, RationalNumber b){
  RationalNumber r = {a.numerator * b.denominator, a.denominator * b.numerator};
  return *normalize(&r);
}

