#include "rationalnumber.h"
#include <stdio.h>

namespace rn {

  /**
   * recursive helper function to find the smallest denominator
   */
  int euclid(int a, int b){
    if(b == 0){
      return a;
    }else{
      return euclid(b, a % b);
    }
  }

  int RationalNumber::num() const{
    return this->numerator;
  }

  int RationalNumber::denom() const{
    return this->denominator;
  }


  /**
   * "normalizes" the given rationalnumber, finding the smallest positive denominator
   */
  void RationalNumber::normalize(){
    int gcd = euclid(this->numerator, this->denominator);
    if(gcd != 0){
      this->numerator /= gcd;
      this->denominator /= gcd;
    }
    if(this->denominator < 0){
      this->numerator *= -1;
      this->denominator *= -1;
    }
  }

  /**
   * Checks if the denominator of a rationalnumber is not 0
   */
  bool RationalNumber::isValid() const{
    return this->denominator != 0;
  }

  /**
   * checks two rationalnumbers for equality
   * normalizes the numbers, so that e.g. 6/8 == 3/4
   */
  bool operator==(const RationalNumber a, const RationalNumber b){
    return a.denom() == b.denom() && a.num() == b.num();
  }

  RationalNumber RationalNumber::inverse() const{
    return RationalNumber(this->denominator, this->numerator);
  }

  RationalNumber RationalNumber::operator-() const{
    return RationalNumber(this->numerator * -1, this->denominator);
  }

  /**
   * compares two rationalnumbers
   * normalizes the numbers, so that e.g. 5/8 < 3/4
   */
  bool operator<(const RationalNumber a, const RationalNumber b){
    return a.num() * b.denom() < b.num() * a.denom();
  }

  /**
   * Adds one rationalnumber to another
   */
  RationalNumber operator+(const RationalNumber a, const RationalNumber b){
    return RationalNumber(a.num() * b.denom() + b.num() * a.denom(), a.denom() * b.denom());
  }

  /**
   * Subtracts one rationalnumber from another
   */
  RationalNumber operator-(RationalNumber a, RationalNumber b){
    return RationalNumber(a.num() * b.denom() - b.num() * a.denom(), a.denom() * b.denom());
  }

  /**
   * Multiplies one rationalnumber with another
   */
  RationalNumber operator*(RationalNumber a, RationalNumber b){
    return RationalNumber(a.num() * b.num(), a.denom() * b.denom());
  }

  /**
   * Divides one rationalnumber with another
   */
  RationalNumber rnDivide(RationalNumber a, RationalNumber b){
    return RationalNumber(a.num() * b.denom(), a.denom() * b.num());
  }

} // namespace rn
