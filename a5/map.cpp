#include "map.h"

namespace rn {

  Map::Map():root(NULL){
  }

  Map::Map(const Map& map):root(NULL){
    *this = map;
  }

  Map& Map::operator=(const Map& map){
    if(this != &map){
      if(root){
        delete root->destroy();
      }

      if(map.root){
        root = map.root->clone();
      }
    }
    return *this;
  }

  Map::~Map(){
    if(this->root){
      delete this->root->destroy();
    }
  }

  Map::Node* Map::Node::clone(){
    Node* copy = new Node(key, 0);
    copy->value = this->value;

    if (left) {
      copy->left = left->clone();
    }

    if (right) {
      copy->right = right->clone();
    }
    return copy;
  }

  std::ostream& operator<<(std::ostream& os, const Map& n){
    os << "Root:" << std::endl;
    os << *(n.root) << std::endl;
    return os;
  }

  mapped_type& Map::operator[](const key_type& key){
    if(root){
      return root->insert(key)->value;
    }else{
      root = new Node(key, 0);
      return root->value;
    }
  }

  Map::Node* Map::Node::insert(const key_type& key){
    if(this->key == key){
      return this;
    }else if(key < this->key){
      if(left){
        return left->insert(key);
      }else{
        left = new Node(key, this);
        return left;
      }
    }else{
      if(right){
        return right->insert(key);
      }else{
        right = new Node(key, this);
        return right;
      }
    }
  }

  Map::Node::Node(const key_type& key, Node* parent){
    this->left = 0;
    this->right = 0;

    this->parent = parent;
    this->key = key;
    this->value = mapped_type();
  }

  Map::Node* Map::Node::destroy(){
    if(left){
      delete left->destroy();
    }
    if (right){
      delete right->destroy();
    }
    return this;
  }

  Map::Node::~Node(){
  }

} // namespace rn
