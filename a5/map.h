#include "rationalnumber.h"
#include <iostream>

namespace rn {
  typedef RationalNumber key_type;
  typedef int mapped_type;
  class Map {
    private:
    class Node{
      friend std::ostream& operator<<(std::ostream& os, const Node& n){
        os << n.key << ":" << n.value << std::endl;
        if (n.left) {
          os << "Left:" << std::endl;
          os << *(n.left) << std::endl;
        }
        if (n.right) {
          os << "Right:" << std::endl;
          os << *(n.right) << std::endl;
        }
        return os;
      }
      Node* left;
      Node* right;

      Node* parent;

      public:
      Node(const key_type& key, Node* parent);
      ~Node();

      Node* destroy();
      key_type key;
      mapped_type value;

      Node* clone();

      Node* insert(const key_type& key);
      Node* find(const key_type& key);
    };
    friend std::ostream& operator<<(std::ostream& output, const Map& n);

    public:
    Map();
    Map(const Map& map);

    ~Map();

    Node* root;

    bool contains(const key_type& key);

    mapped_type& operator[](const key_type& key);
    Map& operator=(const Map& map);


  };
} // namespace rn
