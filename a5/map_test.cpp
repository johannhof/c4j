#include <iostream>
#include "assert.h"
#include "rationalnumber.h"
#include "map.h"

using namespace std;
using namespace rn;

void testInsert(){

  cout << "Testing insertion" << endl;

  Map map = Map();

  RationalNumber rn = RationalNumber(1,2);
  map[rn] = 2;
  assert(map[rn] == 2);

  rn = RationalNumber(1,3);
  map[rn] = 4;
  assert(map[rn] == 4);

  rn = RationalNumber(2,3);
  map[rn] = 42;
  assert(map[rn] == 42);

  map[rn] = 1;
  assert(map[rn] == 1);

  rn = RationalNumber(3,6);
  map[rn] = 42;
  assert(map[rn] == 42);

  rn = RationalNumber(329,6);
  map[rn] = 19;
  assert(map[rn] == 19);

  std::cout << map << std::endl;
  cout << "Insertion test finished" << endl;
}

void testCopying(){

  cout << "Testing Copying" << endl;

  Map map = Map();

  RationalNumber rn = RationalNumber(1,2);
  map[rn] = 2;
  assert(map[rn] == 2);

  rn = RationalNumber(1,3);
  map[rn] = 4;
  assert(map[rn] == 4);

  rn = RationalNumber(2,3);
  map[rn] = 42;
  assert(map[rn] == 42);

  map[rn] = 1;
  assert(map[rn] == 1);

  rn = RationalNumber(3,6);
  map[rn] = 42;
  assert(map[rn] == 42);

  rn = RationalNumber(329,6);
  map[rn] = 19;
  assert(map[rn] == 19);

  Map map2 = map;

  rn = RationalNumber(1,3);
  assert(map[rn] == 4);

  rn = RationalNumber(2,3);
  assert(map[rn] == 1);

  rn = RationalNumber(3,6);
  assert(map[rn] == 42);

  rn = RationalNumber(329,6);
  assert(map[rn] == 19);

  std::cout << map << std::endl;
  //delete &map;

  cout << "Copying test finished" << endl;
}

void testDeletion(){

  cout << "Testing Deletion" << endl;

  Map* map_p = new Map();
  Map map = *(map_p);

  RationalNumber rn = RationalNumber(1,2);
  map[rn] = 2;
  assert(map[rn] == 2);

  rn = RationalNumber(1,3);
  map[rn] = 4;
  assert(map[rn] == 4);

  rn = RationalNumber(2,3);
  map[rn] = 42;
  assert(map[rn] == 42);

  map[rn] = 1;
  assert(map[rn] == 1);

  rn = RationalNumber(3,6);
  map[rn] = 42;
  assert(map[rn] == 42);

  rn = RationalNumber(329,6);
  map[rn] = 19;
  assert(map[rn] == 19);

  delete map_p;

  cout << "Deletion test finished" << endl;
}
int main(void) {
  cout << "Starting unit tests for class Map..." << endl;

  testInsert();
  testCopying();
  testDeletion();

  cout << "Unit tests for class Map finished!" << endl;
  return 0;
}
