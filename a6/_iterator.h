class Iterator {
  private:
    Node* node;

  public:
    Node* last;

    Iterator(Node* node=0):node(node){
    }

    value_t& operator*(){
      return this->node->value;
    }

    value_t* operator->(){
      return &(this->node->value);
    }

    Iterator& operator++(){
      Node* tmp = this->node;
      this->node = node->findNextLarger(node->key);
      if(tmp == this->node){
        this->last = this->node;
        this->node = 0;
      }
      return *this;
    }

    Iterator operator++(int i){
      Node* tmp = this->node;
      this->node = node->findNextLarger(node->key);
      if(tmp == this->node){
        this->last = this->node;
        this->node = 0;
      }
      return Iterator(tmp);
    }

    Iterator& operator--(){
      if(this->last){
        this->node = this->last;
        this->last = 0;
      }else{
        this->node = node->findNextSmaller(node->key);
      }
      return *this;
    }

    Iterator operator--(int i){
      Iterator sub = Iterator(this->node);
      if(this->last){
        this->node = this->last;
        this->last = 0;
      }else{
        this->node = node->findNextSmaller(node->key);
      }
      return sub;
    }

    bool operator==(const Iterator& i){
      return this->node == i.node;
    }

    bool operator!=(const Iterator& i){
      return this->node != i.node;
    }
};

