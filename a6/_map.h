template<class KeyT, class T>
Map<KeyT, T>::Map():root(NULL){
}

template<class KeyT, class T>
Map<KeyT, T>::Map(const Map<KeyT, T>& map):root(NULL){
  *this = map;
}

template<class KeyT, class T>
Map<KeyT, T>& Map<KeyT, T>::operator=(const Map<KeyT, T>& map){
  if(this != &map){
    if(root){
      delete root->destroy();
    }

    if(map.root){
      root = map.root->clone();
    }
  }
  return *this;
}

template<class KeyT, class T>
Map<KeyT, T>::~Map(){
  if(this->root){
    delete this->root->destroy();
  }
}

template<class KeyT, class T>
typename Map<KeyT, T>::Node* Map<KeyT, T>::Node::clone(){
  Node* copy = new Node(key, 0);
  copy->value.second = this->value.second;

  if (left) {
    copy->left = left->clone();
  }

  if (right) {
    copy->right = right->clone();
  }
  return copy;
}

template<class KeyT, class T>
typename Map<KeyT, T>::Node* Map<KeyT, T>::Node::findNextLarger(const key_type& original_key){
  if(this->right && original_key < this->right->key){
    return findFirst(this->right);
  }

  if(!this->parent){
    if(this->key < original_key || this->key == original_key){
      return findLast(this);
    }
    return this;
  }

  if(original_key < this->parent->key){
    return this->parent;
  }

  return this->parent->findNextLarger(original_key);
}

template<class KeyT, class T>
typename Map<KeyT, T>::Node* Map<KeyT, T>::Node::findNextSmaller(const key_type& original_key){
  if(this->left && this->left->key < original_key){
    return findLast(this->left);
  }

  if(!this->parent){
    if(this->key > original_key || this->key == original_key){
      return findFirst(this);
    }
    return this;
  }

  if(this->parent->key < original_key){
    return this->parent;
  }

  return this->parent->findNextSmaller(original_key);
}

template<class KeyT, class T>
typename Map<KeyT, T>::mapped_type& Map<KeyT, T>::operator[](const key_type& key){
  if(root){
    return root->insert(key)->value.second;
  }else{
    root = new Node(key, 0);
    return root->value.second;
  }
}

template<class KeyT, class T>
typename Map<KeyT, T>::Node* Map<KeyT, T>::Node::insert(const key_type& key){
  if(this->key == key){
    return this;
  }else if(key < this->key){
    if(left){
      return left->insert(key);
    }else{
      left = new Node(key, this);
      return left;
    }
  }else{
    if(right){
      return right->insert(key);
    }else{
      right = new Node(key, this);
      return right;
    }
  }
}

template<class KeyT, class T>
Map<KeyT, T>::Node::Node(key_type key, Node* parent)
  :key(key), left(0), right(0), parent(parent), value(value_t(key, mapped_type())){
}

template<class KeyT, class T>
typename Map<KeyT, T>::Node* Map<KeyT, T>::Node::destroy(){
  if(left){
    delete left->destroy();
  }
  if (right){
    delete right->destroy();
  }
  return this;
}

template<class KeyT, class T>
Map<KeyT, T>::Node::~Node(){
}
