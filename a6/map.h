#include <iostream>

template<class KeyT, class T>
class Map {
  public:
    typedef KeyT key_type;
    typedef T mapped_type;
    typedef std::pair<const key_type, mapped_type> value_t;

  private:

    class Node{
      friend std::ostream& operator<<(std::ostream& os, const Node& n){
        os << n.value.first << ":" << n.value.second << std::endl;
        if (n.left) {
          os << "Left:" << std::endl;
          os << *(n.left) << std::endl;
        }
        if (n.right) {
          os << "Right:" << std::endl;
          os << *(n.right) << std::endl;
        }
        return os;
      }

      public:
      Node(key_type key, Node* parent);
      ~Node();

      Node* destroy();
      const key_type key;
      value_t value;

      Node* left;
      Node* right;

      Node* parent;

      Node* clone();

      Node* insert(const key_type& key);
      Node* find(const key_type& key);

      Node* findNextLarger(const key_type& original_key);

      Node* findNextSmaller(const key_type& original_key);
    };

    friend std::ostream& operator<<(std::ostream& os, const Map<KeyT, T>& n){
      os << "Root:" << std::endl;
      os << *(n.root) << std::endl;
      return os;
    }

    static Node* findFirst(Node* start){
      if(start->left){
        return findFirst(start->left);
      }
      return start;
    }

    static Node* findLast(Node* start){
      if(start->right){
        return findLast(start->right);
      }
      return start;
    }

    #include "_iterator.h"

  public:
    Map();
    Map(const Map<KeyT, T>& map);

    ~Map();

    Node* root;

    bool contains(const key_type& key);

    mapped_type& operator[](const key_type& key);
    Map<KeyT, T>& operator=(const Map<KeyT, T>& map);

    Map<KeyT, T>::Iterator begin(){
      return Iterator(findFirst(root));
    }

    Map<KeyT, T>::Iterator end(){
      Iterator i;
      i.last = findLast(root);
      return i;
    }

    typedef Iterator iterator;

};

#include "_map.h"
