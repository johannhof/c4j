#include "assert.h"
#include <iostream>
#include "rationalnumber.h"
#include "map.h"

using namespace std;
using namespace rn;

void testInsert(){

  cout << "Testing insertion" << endl;

  Map<RationalNumber, int> map;

  RationalNumber rn = RationalNumber(1,2);
  map[rn] = 2;
  assert(map[rn] == 2);

  rn = RationalNumber(1,3);
  map[rn] = 4;
  assert(map[rn] == 4);

  rn = RationalNumber(2,3);
  map[rn] = 42;
  assert(map[rn] == 42);

  map[rn] = 1;
  assert(map[rn] == 1);

  rn = RationalNumber(3,6);
  map[rn] = 42;
  assert(map[rn] == 42);

  rn = RationalNumber(329,6);
  map[rn] = 19;
  assert(map[rn] == 19);

  Map<RationalNumber, int> map2 = map;

  std::cout << map << std::endl;
  cout << "Insertion test finished" << endl;
}

void testCopying(){

  cout << "Testing Copying" << endl;

  Map<RationalNumber, int> map;

  RationalNumber rn = RationalNumber(1,2);
  map[rn] = 2;
  assert(map[rn] == 2);

  rn = RationalNumber(1,3);
  map[rn] = 4;
  assert(map[rn] == 4);

  rn = RationalNumber(2,3);
  map[rn] = 42;
  assert(map[rn] == 42);

  map[rn] = 1;
  assert(map[rn] == 1);

  rn = RationalNumber(3,6);
  map[rn] = 42;
  assert(map[rn] == 42);

  rn = RationalNumber(329,6);
  map[rn] = 19;
  assert(map[rn] == 19);

  Map<RationalNumber, int> map2;

  rn = RationalNumber(1,3);
  assert(map[rn] == 4);

  rn = RationalNumber(2,3);
  assert(map[rn] == 1);

  rn = RationalNumber(3,6);
  assert(map[rn] == 42);

  rn = RationalNumber(329,6);
  assert(map[rn] == 19);

  std::cout << map << std::endl;

  cout << "Copying test finished" << endl;
}

void testDeletion(){

  cout << "Testing Deletion" << endl;

  Map<RationalNumber, int>* map_p = new Map<RationalNumber, int>();
  Map<RationalNumber, int> map = *(map_p);

  RationalNumber rn = RationalNumber(1,2);
  map[rn] = 2;
  assert(map[rn] == 2);

  rn = RationalNumber(1,3);
  map[rn] = 4;
  assert(map[rn] == 4);

  rn = RationalNumber(2,3);
  map[rn] = 42;
  assert(map[rn] == 42);

  map[rn] = 1;
  assert(map[rn] == 1);

  rn = RationalNumber(3,6);
  map[rn] = 42;
  assert(map[rn] == 42);

  rn = RationalNumber(329,6);
  map[rn] = 19;
  assert(map[rn] == 19);

  delete map_p;

  cout << "Deletion test finished" << endl;
}

void testString(){

  cout << "Testing generic map with String, Int" << endl;

  Map<std::string, int> map;

  map["Johann"] = 10;
  map["Marie"] = 99;

  map["Marie"] += 1;

  map["zwei"] = 2;
  assert(map["zwei"] == 2);

  map["vier"] = 4;
  assert(map["vier"] == 4);

  std::cout << map << std::endl;
  cout << "Generic map with String, Int test finished" << endl;
}

void testIterator(){

  cout << "Testing Iterator" << endl;

  Map<int,string> m;
  m[4] = "vier";
  m[1] = "eins";
  m[6] = "sechs";
  m[2] = "zwei";
  m[3] = "drei";
  m[9] = "neun";
  m[8] = "acht";

  string sorted[] = { "eins", "zwei", "drei", "vier", "sechs", "acht", "neun" };

  Map<int,string>::iterator it = m.begin();
  Map<int,string>::iterator it2 = m.begin();

  assert(it == it2);

  pair<int, string> t = *it;

  assert(t.second == "eins");

  ++it2;

  assert(it2->second == "zwei");

  int x = 0;

  std::cout << m << std::endl;

  Map<int,string>::iterator end_it = m.end();
  while(it != end_it) {
    std::cout << it->second << ":" << sorted[x] << std::endl;
    assert(it++->second == sorted[x++]);
  }

  x = 6;
  it--;
  std::cout << it->second << std::endl;
  assert(it->second == sorted[x]);

  do{
    assert(it--->second == sorted[x--]);
    std::cout << it->second << ":" << sorted[x] << std::endl;
  }while(it != m.begin());

  cout << "Iterator test finished" << endl;
}

void testSchirm(){
  Map<int,string> m;
  m[4] = "vier";
  m[7] = "sieben";
  m[2] = "zwei";
  Map<int, string>::iterator i = m.begin();
  cout << "kleinstes Element: " << i->first << "/" << i->second << endl;

  cout << "weitere Elemente, sortiert: " << endl;
  while(i!=m.end()) {
    pair<int,string> p = *i;
    cout << " " << p.first << "/" << p.second << endl;
    i++;
  }
  m.begin()->second = "neuer Wert";
  //m.begin()->first = 9;
}

int main(void) {
  cout << "Starting unit tests for class Map..." << endl;

  testInsert();
  testCopying();
  testDeletion();
  testString();
  testIterator();
  testSchirm();

  cout << "Unit tests for class Map finished!" << endl;
  return 0;
}
