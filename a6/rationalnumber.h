#ifndef _RATIONALNUMBER
#define _RATIONALNUMBER

#include <iostream>

namespace rn {

  class RationalNumber{
    int numerator;
    int denominator;
    void normalize();

    friend std::ostream& operator<<(std::ostream& output, const RationalNumber& n);

    public:

    RationalNumber(int n=0, int d=1):numerator(n), denominator(d){
      this->normalize();
    };

    int num() const;

    int denom() const;

    bool isValid() const;

    RationalNumber inverse() const;

    RationalNumber operator-() const;

    RationalNumber subtract(RationalNumber a, RationalNumber b);

    RationalNumber multiply(RationalNumber a, RationalNumber b);

    RationalNumber divide(RationalNumber a, RationalNumber b);
  };

  bool operator==(const RationalNumber a, const RationalNumber b);
  bool operator<(const RationalNumber a, const RationalNumber b);
  RationalNumber operator+(const RationalNumber a, const RationalNumber b);

}

#endif
