#include "rationalnumbercollection.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
 RationalNumber number;
 int count;
} Element;

struct RationalNumberCollection{
  Element* arr;
  int index;
  int total;
  int max;
  int size_step;
  RationalNumber sum;
};

void rncInit(RationalNumberCollection* rnc);

/**
 * Creates a new Rationalnumbercollection and returns a pointer to it
 * The object is created by using malloc() and must be free()d when it is
 * no longer needed
 */
RationalNumberCollection* rncCreate(int size){
  RationalNumberCollection* rnc = (RationalNumberCollection*) malloc(sizeof(RationalNumberCollection));
  rncInit(rnc);
  rnc->max = size;
  rnc->size_step = size;
  rnc->arr = (Element*) malloc(sizeof(Element) * size);
  return rnc;
}

/**
 * Inserts an element at the specified position, sorting the array
 * If the Rationalnumbercollection is not big enough to hold the element,
 * it will be made bigger by the factor that was specified as the original size
 */
void insert(RationalNumberCollection* rnc, Element e, int pos){
  if(rnc->index >= rnc->max - 1){
    void* new_arr = malloc(sizeof(Element) * (rnc->max + rnc->size_step));
    memcpy(new_arr, rnc->arr, sizeof(Element) * rnc->max);
    rnc->max += rnc->size_step;
    free(rnc->arr);
    rnc->arr = (Element*) new_arr;
  }

  Element* cur = &(rnc->arr[pos]);
  Element* last = &(rnc->arr[rnc->index]);

  while(last --> cur){
    *(last+1) = *(last);
  }

  rnc->arr[pos] = e;
  rnc->index++;
  rnc->total += 1;
  rnc->sum = rnAdd(rnc->sum, e.number);
}

/**
 * Frees the memory that is occupied by the given Rationalnumbercollection
 */
void rncDelete(RationalNumberCollection* rnc){
  free(rnc->arr);
  free(rnc);
}

/**
 * Initializes a new Rationalnumbercollection by filling it with default values
 * You need to call this when creating a new rnc
 */
void rncInit(RationalNumberCollection* rnc){
  rnc->index = 0;
  rnc->total = 0;
  RationalNumber n = {0,1};
  rnc->sum = n;
}

/**
 * Finds the index of a rationalnumber in a rationalnumbercollection
 */
int find(RationalNumberCollection* rnc, RationalNumber n){
  int center,
      left = 0,
      right = rnc->index - 1;

  while (left <= right){
    center = left + (right - left) / 2;

    if (rnEqual(rnc->arr[center].number,n)){
      return center;
    }else{
      if (rnLessThan(n,rnc->arr[center].number)){
        right = center - 1;
      }else{
        left = center + 1;
      }
    }
  }

  return -1;
}

/**
 * Returns the total count of numbers in a Rationalnumbercollection
 */
int rncTotalCount(RationalNumberCollection* rnc){
  return rnc->total;
}

/**
 * Returns the total count of unique numbers in a Rationalnumbercollection
 */
int rncTotalUniqueCount(RationalNumberCollection* rnc){
  return rnc->index;
}

/**
 * Returns the sum of numbers in a Rationalnumbercollection
 */
RationalNumber rncSum(RationalNumberCollection* rnc){
  return rnc->sum;
}

/**
 * Returns the average of numbers in a Rationalnumbercollection
 */
RationalNumber rncAverage(RationalNumberCollection* rnc){
  RationalNumber n = {rnc->total, 1};
  return rnDivide(rnc->sum, n);
}

/**
 * Removes a number from a Rationalnumbercollection
 * If the number exists more than once, it reduces the count by 1
 */
void rncRemove(RationalNumberCollection* rnc, RationalNumber n){
  int index = find(rnc, n);
  if(~index && !--rnc->arr[index].count){
    Element* cur = &(rnc->arr[index]);
    Element* last = &(rnc->arr[rnc->index]);

    do{
      *(cur) = *(cur + 1);
    }while(cur++ < last);
  }
}

/**
 * Returns the number of times a rationalnumber exists in a Rationalnumbercollection
 */
int rncCount(RationalNumberCollection* rnc, RationalNumber n){
  int index = find(rnc, n);
  if(~index){
    return rnc->arr[index].count;
  }else{
    return 0;
  }
}

/**
 * Prints the contents of a Rationalnumbercollection to the screen
 */
void rncPrint(RationalNumberCollection* rnc){
  Element* cur = rnc->arr;
  Element* last = &(rnc->arr[rnc->index]);
  printf("RNC:\n");
  printf("%d elements\n", rnc->index);
  while(cur < last){
    printf("%d/", cur->number.numerator);
    printf("%d\n", cur->number.denominator);
    printf("\n");
    cur++;
  }
}

/**
 * Adds a number to a Rationalnumbercollection
 */
void rncAdd(RationalNumberCollection* rnc, RationalNumber n){
  int index = find(rnc, n);
  if(~index){
    rnc->arr[index].count += 1;
    rnc->total += 1;
    rnc->sum = rnAdd(rnc->sum, n);
  }else{
    Element e = {n, 1};
    int i;
    for (i = 0; i < rnc->index; i++) {
      if(rnLessThan(n, rnc->arr[i].number)){
        insert(rnc, e, i);
        return;
      }
    }
    insert(rnc, e, rnc->index);
  }
}
