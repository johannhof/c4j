#include "rationalnumber.h"

typedef struct RationalNumberCollection RationalNumberCollection;

RationalNumberCollection* rncCreate(int size);

void rncDelete(RationalNumberCollection* rnc);

int rncCount(RationalNumberCollection* rnc, RationalNumber n);

int rncTotalCount(RationalNumberCollection* rnc);

int rncTotalUniqueCount(RationalNumberCollection* rnc);

RationalNumber rncSum(RationalNumberCollection* rnc);

void rncRemove(RationalNumberCollection* rnc, RationalNumber n);

RationalNumber rncAverage(RationalNumberCollection* rnc);

void rncAdd(RationalNumberCollection* rnc, RationalNumber n);

void rncPrint(RationalNumberCollection* rnc);
